FROM libreoffice/online:latest

USER root

# Install curl and unzip
RUN apt-get update && apt-get install -y curl unzip

# Get Xuxen extension for LibreOffice
RUN curl -o dict-eu.zip "https://extensions.libreoffice.org/assets/downloads/z/xuxen-5-1-libreoffice.oxt" \
# Unzip Xuxen extension
&& unzip dict-eu.zip -d dict-eu \
# Delete Xuxen extension
&& rm -f dict-eu.zip \
# Move dict-eu to the extensions directory
&& mv dict-eu/ /opt/libreoffice/share/extensions/

USER lool
