# LibreOffice Online + Xuxen

[iametza](https://iametza.eus)k garatutako Dockerfile honek Xuxen integratzen du [LibreOffice Online](https://www.libreoffice.org/download/libreoffice-online/)n.

[Xuxen](http://xuxen.eus) [Elhuyar Fundazioa](https://www.elhuyar.eus/)ren eta Euskal Herriko Unibertsitateko [Ixa taldea](http://ixa.si.ehu.es/)ren artean garatutako euskarazko zuzentzaile ortografiko eta gramatikala da.

Oinarri bezala LibreOffice Online proiektuaren Docker irudia erabiltzen dugu: https://hub.docker.com/r/libreoffice/online

## Ezaugarriak
 - Xuxen automatikoki deskargatu, erauzi eta kontainer barruko fitxategi-sisteman dagokion lekura kopiatzen du LibreOffice Online-n erabili ahal izateko.

## Irudia nola eraiki

Exekutatu Dockerfile-a dagoen karpetan:
``` docker image build -t iametza/libreoffice-online-xuxen:master . ```

Edo:
``` docker image build -t iametza/libreoffice-online-xuxen:latest-81b12823616c . ```

## Kontainerra martxan jartzeko
Irudia ```docker pull iametza/libreoffice-online-xuxen:master``` edo ```docker pull iametza/libreoffice-online-xuxen:latest-81b12823616c``` komandoarekin eskuratu edo aurreko pausoan azaldu bezala eraiki ondoren, exekutatu:
``` docker run -t -d -p 127.0.0.1:9980:9980 -e 'domain=azpi-domeinua\\.domeinua\\.eus' -e 'extra_params=--o:allowed_languages="eu es_ES fr_FR en_GB en_US"' --restart always --cap-add MKNOD iametza/libreoffice-online-xuxen:master ```

edo
``` docker run -t -d -p 127.0.0.1:9980:9980 -e 'domain=azpi-domeinua\\.domeinua\\.eus' -e 'extra_params=--o:allowed_languages="eu es_ES fr_FR en_GB en_US"' --restart always --cap-add MKNOD iametza/libreoffice-online-xuxen:latest:81b12823616c ```


### Oharrak
- *GARRANTZITSUA*: **allowed_languages parametro gehigarrian euskara (eu) jarri behar da. Bestela euskara ez da LibreOffice Onlineko hizkuntzen artean agertuko.** Euskaraz gain LibreOfficen zuzentzaile ortografikoa duten beste hizkuntzak ere gehitu daitezke: https://wiki.documentfoundation.org/Development/Dictionaries (gehienak ez ditugu probatu).
- domain parametroan Nextcloud instalatuta daukagun oinarrizko URLa jarri behar da, puntuei aurretik \\\\ jarriz (Plesk erabiltzen baduzu ez jarri).
- Irudiak onartzen dituen parametro gehigarrien (extra_params) dokumentaziorik ez dugu aurkitu, iturburu-kodean begiratu behar izan dugu, hauek direla uste dugu: https://github.com/LibreOffice/online/blob/master/wsd/LOOLWSD.cpp#L809 Norbaitek dokumentaziorik aurkituko balu jakinarazi mesedez.

## Lizentziak
LibreOfficerako Xuxenek GNU GPLv2 lizentzia du: https://extensions.libreoffice.org/extensions/xuxen-5-zuzentzaile-ortografikoa

Proiektu hau [GNU GPLv3](http://www.gnu.org/licenses/gpl-3.0.html) lizentzia duen software librea da.

<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License version 3" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-127x51.png" /></a>
